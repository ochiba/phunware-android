package net.opix.phun.app.model

import net.opix.phun.app.room.DatabaseHelper
import net.opix.phun.app.service.EventService
import net.opix.phun.app.service.RepositoryRequestStatus
import net.opix.phun.app.service.RepositoryResult
import net.opix.phun.app.service.ServiceFactory
import java.lang.Exception

class EventsRepository(private val service: EventService = ServiceFactory.createService()) {
    private var cachedEvents = emptyList<Event>()

    suspend fun getEventDtos(): List<EventDto> {
        return service.getEvents()
    }

    suspend fun getEvents(dbHelper: DatabaseHelper): RepositoryResult<List<Event>> {
        return try {
            val tempo = getEventDtos()
            cachedEvents = tempo.map { event -> Event.fromDto(event) }.sortedByDescending { event -> event.date }

            val entities = tempo.map { event -> event.toEntity() }
            dbHelper.insertAll(entities)

            RepositoryResult(cachedEvents, RepositoryRequestStatus.COMPLETE)
        } catch (e: Exception) {
            cachedEvents = dbHelper.getEvents().map { event -> Event.fromEntity(event) }.sortedByDescending { event -> event.date }

            if (cachedEvents.count() == 0)
                RepositoryResult(emptyList(), RepositoryRequestStatus.Error(e))
            else
                RepositoryResult(cachedEvents, RepositoryRequestStatus.COMPLETE)
        }
    }

    fun getEvent(id: Int): Event? {
        return cachedEvents.firstOrNull {
            it.id == id
        }
    }

    companion object {
        @Volatile
        private var instance: EventsRepository? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: EventsRepository(ServiceFactory.createService()).also { instance = it }
        }
    }
}