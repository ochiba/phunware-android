package net.opix.phun.app.room

interface DatabaseHelper {
    suspend fun getEvents(): List<EventEntity>
    suspend fun insertAll(events: List<EventEntity>)
    suspend fun deleteAll()
}