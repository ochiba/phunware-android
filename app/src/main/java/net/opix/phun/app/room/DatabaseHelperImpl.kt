package net.opix.phun.app.room

class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {
    override suspend fun getEvents(): List<EventEntity> = appDatabase.eventDao().getAll()
    override suspend fun insertAll(events: List<EventEntity>) = appDatabase.eventDao().insertAll(events)
    override suspend fun deleteAll() = appDatabase.eventDao().deleteAll()
}