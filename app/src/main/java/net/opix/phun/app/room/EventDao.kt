package net.opix.phun.app.room

import androidx.room.*

@Dao
interface EventDao {
    @Query("SELECT * FROM EventEntity")
    suspend fun getAll(): List<EventEntity>

    @Query("DELETE FROM EventEntity")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(events: List<EventEntity>)

    @Delete
    suspend fun delete(event: EventEntity)
}