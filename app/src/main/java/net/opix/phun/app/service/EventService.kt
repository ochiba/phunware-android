package net.opix.phun.app.service

import net.opix.phun.app.model.EventDto
import retrofit2.http.GET

interface EventService {
    @GET("feed.json")
    suspend fun getEvents(): List<EventDto>
}