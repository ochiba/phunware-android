package net.opix.phun.app.service

data class RepositoryResult<T>(
    val data: T,
    val requestStatus: RepositoryRequestStatus
)