package net.opix.phun.app.ui

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.ExperimentalUnitApi
import coil.annotation.ExperimentalCoilApi
import net.opix.phun.app.model.Event
import net.opix.phun.app.ui.theme.defaultHalfPadding

@ExperimentalUnitApi
@ExperimentalCoilApi
@Composable
fun EventRowDouble(event1: Event, event2: Event?, navigationCallback: (Int) -> Unit) {
    Row (horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier.fillMaxWidth()) {
        Column (Modifier.weight(1f).padding(end = defaultHalfPadding)) {
            EventRow(event1, navigationCallback)
        }
        Column (Modifier.weight(1f).padding(start = defaultHalfPadding)) {
            if (event2 != null)
                EventRow(event2, navigationCallback)
        }
    }
}