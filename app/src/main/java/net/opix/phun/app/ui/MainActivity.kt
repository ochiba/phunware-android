package net.opix.phun.app.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.annotation.ExperimentalCoilApi
import net.opix.phun.app.ui.theme.PhunAppComposeTheme
import net.opix.phun.app.R
import net.opix.phun.app.model.Event
import net.opix.phun.app.room.DatabaseBuilder
import net.opix.phun.app.room.DatabaseHelperImpl
import net.opix.phun.app.service.RepositoryRequestStatus
import net.opix.phun.app.ui.misc.ShowCircularProgress
import net.opix.phun.app.ui.misc.ShowError
import net.opix.phun.app.ui.theme.defaultPadding
import net.opix.phun.app.viewmodels.EventDetailsViewModel
import net.opix.phun.app.viewmodels.EventsViewModel

class MainActivity : ComponentActivity() {
    @ExperimentalUnitApi
    @ExperimentalCoilApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            PhunAppComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    EventScreen()
                }
            }
        }
    }
}

@ExperimentalUnitApi
@ExperimentalCoilApi
@Composable
private fun EventScreen() {
    val navigationController = rememberNavController()
    NavHost(navController = navigationController, startDestination = "event_list") {
        composable(route = "event_list") {
            EventListScreen { eventId ->
                navigationController.navigate("event_details/$eventId")
            }
        }

        composable(
            route = "event_details/{event_id}",
            arguments = listOf(navArgument("event_id") {
                type = NavType.IntType
            })
        ) {
            val eventDetailsViewModel: EventDetailsViewModel = viewModel()
            EventDetailsPage(eventDetailsViewModel, onBack = {
                navigationController.popBackStack()
            })
        }
    }
}

@ExperimentalUnitApi
@ExperimentalCoilApi
@Composable
fun EventListScreen(navigationCallback: (Int) -> Unit) {
    val eventViewModel: EventsViewModel = viewModel()
    val dbHelper = DatabaseHelperImpl(DatabaseBuilder.getInstance(LocalContext.current))

    eventViewModel.fetchEvents(dbHelper)
    val allEvents = eventViewModel.eventsState

    when (allEvents.requestStatus) {
        is RepositoryRequestStatus.FETCHING -> ShowCircularProgress()
        is RepositoryRequestStatus.COMPLETE -> Content(allEvents.data, navigationCallback)
        is RepositoryRequestStatus.Error -> ShowError()
    }
}

@ExperimentalUnitApi
@ExperimentalCoilApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PhunAppComposeTheme {
        EventListScreen {

        }
    }
}

@ExperimentalUnitApi
@ExperimentalCoilApi
@Composable
fun Content(list: List<Event>, navigationCallback: (Int) -> Unit) {
    val configuration = LocalConfiguration.current
    val isTablet = configuration.screenWidthDp.dp >= 600.dp

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.app_name),
                        color = Color.White
                    )
                },
                navigationIcon = null,
                backgroundColor = colorResource(id = R.color.top_app_bar),
                contentColor = Color.White,
                elevation = 12.dp
            )
        }, content = {
            LazyColumn (contentPadding = PaddingValues(defaultPadding)) {
                if (isTablet) {
                    itemsIndexed(list) { index, item ->
                        val i = index * 2

                        if (i < list.count()) {
                            val i2 = i + 1

                            val event2: Event? = if (i2 >= list.count()) null else list[i2]
                            EventRowDouble(list[i], event2, navigationCallback)
                        }
                    }
                } else {
                    items(list) { event ->
                        EventRow(event, navigationCallback)
                    }
                }
            }
        })
}