package net.opix.phun.app.viewmodels

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import net.opix.phun.app.model.Event
import net.opix.phun.app.model.EventsRepository

class EventDetailsViewModel(private val savedStateHandle: SavedStateHandle): ViewModel() {
    var eventState = mutableStateOf<Event?>(null)
    private val repository: EventsRepository = EventsRepository.getInstance()

    init {
        val eventId = savedStateHandle.get<Int>("event_id") ?: 0
        eventState.value = repository.getEvent(eventId)
    }

    val event: Event?
        get() {
            return eventState.value
        }

    fun getPhone(): String? {
        return event?.phone
    }

    fun generateMessage(): String? {
        event?.let { event ->
            var message = event.title
            message += System.lineSeparator()
            message += System.lineSeparator()
            message += event.dateAsString
            message += System.lineSeparator()
            message += System.lineSeparator()
            message += event.phone ?: ""
            message += System.lineSeparator()
            message += System.lineSeparator()
            message += event.description ?: ""

            return message
        } ?: return null
    }
}