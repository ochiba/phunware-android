package net.opix.phun.app.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import net.opix.phun.app.model.EventsRepository
import net.opix.phun.app.model.Event
import net.opix.phun.app.room.DatabaseHelper
import net.opix.phun.app.service.RepositoryRequestStatus
import net.opix.phun.app.service.RepositoryResult

class EventsViewModel(private val repository: EventsRepository = EventsRepository.getInstance()) :
    ViewModel() {
    private val mutableEventsState: MutableState<RepositoryResult<List<Event>>> = mutableStateOf(RepositoryResult(emptyList(), RepositoryRequestStatus.FETCHING))
    // This is exposed as immutable in MainActivity.  mutableEventsState above is private so MainActivity will not see directly.
    // Another approach:
    // private val resultState: State<RepositoryResult<List<Event>>> = mutableEventsState
    val eventsState: RepositoryResult<List<Event>>
        get() {
            return mutableEventsState.value
        }

    fun fetchEvents(dbHelper: DatabaseHelper) {
        viewModelScope.launch(Dispatchers.IO) {
            mutableEventsState.value = repository.getEvents(dbHelper)
        }
    }
}