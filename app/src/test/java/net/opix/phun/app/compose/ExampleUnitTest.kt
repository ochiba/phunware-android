package net.opix.phun.app.compose

import net.opix.phun.app.extensions.dateFromISO8601WithMilliseconds
import net.opix.phun.app.extensions.localDateAsString
import org.junit.Test
import org.junit.Assert.*

class ExampleUnitTest {
    @Test
    // UTC -> PST
    fun utcToPstConversion() {
        val gmtDateString = "2015-06-26T10:50:54.161Z"
        val gmtDate = gmtDateString.dateFromISO8601WithMilliseconds

        // Note: UTC is 7 hours ahead of PST.
        assertEquals("2015-06-26T03:50:54.161Z", gmtDate?.localDateAsString)

        val gmtDateString2 = "2015-07-08T19:09:00.000Z"
        val gmtDate2 = gmtDateString2.dateFromISO8601WithMilliseconds
        assertEquals("2015-07-08T12:09:00.000Z", gmtDate2?.localDateAsString)
    }
}